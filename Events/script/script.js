// ===============THEORY=================

/*
1. Що таке події в JavaScript і для чого вони використовуються?

Подія - це те, що тим чи іншим чином виникає як явище(дія, подія) в DOM елементі і таким чином задає документу динамічності.
Вона є важливою в JS оскільки без них сайт втрачає динамічний функціонал і свою суть загалом.
Переходи, випливаючі вікна, натискання на кнопки та їх подальша функція і являються подіями. 

2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
click - клік на ліву кнопку миші;
contextmenu - клік на праву кнопку миші;
mousedown - натискання на кнопку миші;
mouseup - відпускання кнопки миші;
mouseover - захід курсора на об'єкт (дочірній також);
mouseout - покидання курсором об'єкта (дочірній також);
mouseenter - захід курсора на об'єкт (лише зазначений обєкт);
mouseleave - покидання курсором об'єкта (лише зазначений обєкт);
mousemove - рух курсора по об'єкту


3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?

contextmenu - це натискання на праву кнопку миші для того щоб відкрити контекстне меню.

*/

// ============PRACTICE=================

// 1.

const section = document.getElementById('content');
const btn = document.getElementById('btn-click');
const paragraph = document.createElement('p');
paragraph.textContent = "New Paragraph";

btn.addEventListener("click", function() {
	btn.after(paragraph);
});


// 2.

const button = document.createElement('button');
button.setAttribute('id', 'btn-input-create');

button.innerHTML = "Click me 2";
button.style.cssText = "display: block; margin: 30px auto; background-color: #007bff; padding: 10px 20px; border: none; color: #fff;";

section.append(button);

button.addEventListener("click", function() {
	const inp = document.createElement('input');
	inp.setAttribute('type', 'text');
	inp.setAttribute('placeholder', 'Please, write here!');

	button.after(inp);
}, {once: true});

